import './container-popover.less';
import { useCls } from '../../hooks';
import React, { useEffect, useState } from 'react';

type ContainerPopoverDirection = 'rl' | 'bt' | 'lr' | 'tb';

export interface ContainerPopoverProps {
  children: React.ReactNode;
  direction: ContainerPopoverDirection;
  isOpened: boolean;
}

export const ContainerPopover: React.FC<ContainerPopoverProps> = ({
  children,
  direction,
  isOpened,
}) => {
  const cls = useCls('container-popover');
  const [isHidden, setIsHidden] = useState(true);

  useEffect(() => {
    if (!isOpened) {
      setTimeout(() => {
        setIsHidden(true);
      }, 250);
    } else {
      setIsHidden(false);
    }
  }, [isOpened]);

  return (
    <div
      className={cls({
        direction,
        'is-hidden': isHidden,
      })}
    >
      {children}
    </div>
  );
};

export default ContainerPopover;
