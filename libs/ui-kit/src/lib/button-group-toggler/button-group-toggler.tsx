import './button-group-toggler.less';
import React from 'react';
import { Badge, Button } from 'antd';
import { useCls } from '../../hooks/useCls';

interface Toggler {
  name: string;
  count?: number;
}

export interface ButtonGroupTogglerProps {
  togglers: Toggler[];
  currentTogglerIndex: number;
  setCurrentTogglerIndex: (newIndex: number) => void;
}

export const ButtonGroupToggler: React.FC<ButtonGroupTogglerProps> = ({
  togglers,
  currentTogglerIndex,
  setCurrentTogglerIndex,
}) => {
  const cls = useCls('button-group-toggler');

  return (
    <div className={cls()}>
      {togglers.map(({ name, count }, index) => (
        <Button
          key={name}
          className={cls('button')}
          type={currentTogglerIndex === index ? 'primary' : 'text'}
          onClick={() => setCurrentTogglerIndex(index)}
        >
          <Badge
            size={'small'}
            count={count}
            className={cls('badge', {
              active: currentTogglerIndex === index,
            })}
          >
            {name}
          </Badge>
        </Button>
      ))}
    </div>
  );
};

export default ButtonGroupToggler;
