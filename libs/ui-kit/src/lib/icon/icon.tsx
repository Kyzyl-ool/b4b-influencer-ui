import './icon.less';
import React from 'react';
import { ColorNames } from '../../colors/colors';
import { useCls } from '../../hooks/useCls';

export interface IconProps extends React.HTMLProps<HTMLSpanElement> {
  children: React.ReactNode;
  color?: ColorNames;
}

export const Icon: React.FC<IconProps> = ({ children, color, ...rest }) => {
  const cls = useCls('icon');

  return (
    <span className={cls(color || '')} {...rest}>
      {children}
    </span>
  );
};

export default Icon;
