import './container-base-layer.less';
import { useCls } from '../../hooks';
import React from 'react';
import classnames from 'classnames';

export interface ContainerBaseLayerProps
  extends React.HTMLProps<HTMLDivElement> {
  children: React.ReactNode;
}

export const ContainerBaseLayer: React.FC<ContainerBaseLayerProps> = ({
  children,
  className,
  ...rest
}) => {
  const cls = useCls('container-base-layer');

  return (
    <div className={classnames(cls(), className)} {...rest}>
      {children}
    </div>
  );
};

export default ContainerBaseLayer;
