import './balance.less';
import React from 'react';
import { useCls } from '../../hooks';
import Icon from '../icon/icon';
import { B4BCoinIcon, WalletIcon } from '../icons';
import { Typography } from 'antd';

const { Title } = Typography;

export interface BalanceProps {
  children: React.ReactNode;
}

export const Balance: React.FC<BalanceProps> = ({ children }) => {
  const cls = useCls('balance');

  return (
    <div className={cls()}>
      <Icon>
        <WalletIcon />
      </Icon>
      <span className={cls('value-container')}>
        <Icon>
          <B4BCoinIcon />
        </Icon>
        <Title level={4} className={cls('value')}>
          {children}
        </Title>
      </span>
    </div>
  );
};

export default Balance;
