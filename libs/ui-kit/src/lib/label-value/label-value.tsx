import './label-value.less';
import { useCls } from '../../hooks';
import React from 'react';

export interface LabelValueProps {
  label: React.ReactNode;
  value: React.ReactNode;
}

export const LabelValue: React.FC<LabelValueProps> = ({ value, label }) => {
  const cls = useCls('label-value');

  return (
    <div className={cls()}>
      <div className={cls('label')}>{label}</div>
      <div className={cls('value')}>{value}</div>
    </div>
  );
};

export default LabelValue;
