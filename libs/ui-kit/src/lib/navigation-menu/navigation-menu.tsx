import './navigation-menu.less';
import React from 'react';
import { useCls } from '../../hooks';
import { Link } from 'react-router-dom';

interface NavigationItem {
  title: React.ReactNode;
  route: string;
}
export interface NavigationMenuProps {
  items: NavigationItem[];
}

export const NavigationMenu: React.FC<NavigationMenuProps> = ({ items }) => {
  const cls = useCls('navigation-menu');

  return (
    <div className={cls()}>
      {items.map(({ route, title }) => (
        <div key={route}>
          <Link to={route}>{title}</Link>
        </div>
      ))}
    </div>
  );
};

export default NavigationMenu;
