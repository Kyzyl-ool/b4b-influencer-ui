import './container-base-page.less';
import React from 'react';
import { useCls } from '../../hooks/useCls';
import classnames from 'classnames';

export interface ContainerBasePageProps
  extends React.HTMLProps<HTMLDivElement> {
  children: React.ReactNode;
  footer?: React.ReactNode;
}

export const ContainerBasePage: React.FC<ContainerBasePageProps> = ({
  children,
  footer,
  className,
}) => {
  const cls = useCls('container-base-page');

  return (
    <div className={cls()}>
      <div className={classnames(cls('content'), className)}>{children}</div>
      {footer && <div className={cls('footer')}>{footer}</div>}
    </div>
  );
};

export default ContainerBasePage;
