import './coins-balance.less';
import LabelValue from '../label-value/label-value';
import { useCls } from '../../hooks';
import React from 'react';
import { B4BCoinIcon, B4BPointsIcon, HederaCoinIcon, USDCIcon } from '../icons';
import { Typography } from 'antd';
import ContainerBaseLayer from '../container-base-layer/container-base-layer';

const { Text } = Typography;

const mapTickerToIcon: Record<string, React.ReactNode> = {
  USDC: <USDCIcon />,
  B4BP: <B4BPointsIcon />,
  B4BC: <B4BCoinIcon />,
  HBAR: <HederaCoinIcon />,
};

interface Coin {
  ticker: string;
  balance: string;
}

export interface CoinsBalanceProps {
  coins: Coin[];
}

export const CoinsBalance: React.FC<CoinsBalanceProps> = ({ coins }) => {
  const cls = useCls('coins-balance');

  return (
    <ContainerBaseLayer className={cls()}>
      {coins.map(({ balance, ticker }) => (
        <LabelValue
          key={ticker}
          label={
            <span className={cls('label')}>
              {ticker in mapTickerToIcon ? mapTickerToIcon[ticker] : null}
              <Text>{ticker}</Text>
            </span>
          }
          value={balance}
        />
      ))}
    </ContainerBaseLayer>
  );
};

export default CoinsBalance;
