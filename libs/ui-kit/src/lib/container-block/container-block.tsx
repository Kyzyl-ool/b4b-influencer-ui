import './container-block.less';
import React from 'react';
import { useCls } from '../../hooks/useCls';
import classnames from 'classnames';

export interface ContainerBlockProps extends React.HTMLProps<HTMLDivElement> {
  centered?: boolean;
}

export const ContainerBlock: React.FC<ContainerBlockProps> = ({
  children,
  centered,
  className,
}) => {
  const cls = useCls('container-block');

  return (
    <div
      className={classnames(
        cls({
          centered,
        }),
        className
      )}
    >
      {children}
    </div>
  );
};

export default ContainerBlock;
