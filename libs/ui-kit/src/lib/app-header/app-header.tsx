import './app-header.less';
import { B4BIcon, MenuIcon } from '../icons';
import { useCls } from '../../hooks/useCls';
import Balance from '../balance/balance';
import Icon from '../icon/icon';
import React from 'react';

export interface AppHeaderProps {
  onRightIconClick: React.MouseEventHandler<HTMLSpanElement>;
  rightIcon?: React.ReactNode;
  middleContent?: React.ReactNode;
}

export const AppHeader: React.FC<AppHeaderProps> = ({
  onRightIconClick,
  rightIcon,
  middleContent,
}) => {
  const cls = useCls('app-header');

  return (
    <div className={cls()}>
      <B4BIcon />
      <span>{middleContent ?? <Balance>1000</Balance>}</span>
      <Icon onClick={onRightIconClick}>
        {rightIcon ? rightIcon : <MenuIcon />}
      </Icon>
    </div>
  );
};

export default AppHeader;
