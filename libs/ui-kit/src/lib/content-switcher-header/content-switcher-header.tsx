import './content-switcher-header.less';
import { useCls } from '../../hooks';
import React from 'react';
import { Typography } from 'antd';
import { ChevronLeftIcon, ChevronRightIcon } from '../icons';
import Icon from '../icon/icon';

const { Title } = Typography;

export interface ContentSwitcherHeaderProps {
  currentTitle: string;
  leftDisabled?: boolean;
  rightDisabled?: boolean;
  onClickLeft?: React.MouseEventHandler<HTMLSpanElement>;
  onClickRight?: React.MouseEventHandler<HTMLSpanElement>;
}

export const ContentSwitcherHeader: React.FC<ContentSwitcherHeaderProps> = ({
  currentTitle,
  rightDisabled,
  leftDisabled,
  onClickRight,
  onClickLeft,
}) => {
  const cls = useCls('content-switcher-header');

  return (
    <div className={cls()}>
      <div className={cls('icon')}>
        {leftDisabled && (
          <Icon onClick={onClickLeft}>
            <ChevronLeftIcon />
          </Icon>
        )}
      </div>
      <Title level={4} className={cls('header')}>
        {currentTitle}
      </Title>
      <div className={cls('icon')}>
        {rightDisabled && (
          <Icon onClick={onClickRight}>
            <ChevronRightIcon />
          </Icon>
        )}
      </div>
    </div>
  );
};

export default ContentSwitcherHeader;
