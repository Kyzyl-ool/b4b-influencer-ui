import './ad-campaign-card.less';
import React from 'react';
import { useCls } from '../../hooks/useCls';
import { Button, Col, Form, Input, Row, Typography } from 'antd';
import { BriefLinkIcon } from '../icons';
import Icon from '../icon/icon';
import classnames from 'classnames';
import Countdown from '../countdown/countdown';
import { hashconnectService } from '../../../../../apps/b4b-hedera-hack-ui/src/hashconnect';

const { Text, Link } = Typography;

export interface AdCampaignCardProps extends React.HTMLProps<HTMLDivElement> {
  brandName: string;
  href: string;
  linkText: string;
  briefHref: string;
  date: Date;
  reward: string;
  b4bReward: number;
  dueTo: Date;
  color?: string;
  isActionsVisible?: boolean;
  isLinkFormVisible?: boolean;
}

export const AdCampaignCard: React.FC<AdCampaignCardProps> = ({
  dueTo,
  brandName,
  briefHref,
  href,
  date,
  linkText,
  reward,
  b4bReward,
  className,
  color = 'purple',
  isActionsVisible = true,
  isLinkFormVisible = false,
  ...rest
}) => {
  const cls = useCls('ad-campaign-card');

  return (
    <div
      className={classnames(
        cls({
          color,
        }),
        className
      )}
      {...rest}
    >
      <div className={cls('brand-info')}>
        <Text>
          <b>{brandName}</b>
        </Text>
        <Link href={href}>{briefHref}</Link>
      </div>
      <div className={cls('brief-info')}>
        <Link className={cls('brief-link')}>
          Brief&nbsp;
          <Icon>
            <BriefLinkIcon />
          </Icon>
        </Link>
        <Text>{date.toLocaleDateString()}</Text>
        <Text>
          <b>{reward}</b>
        </Text>
      </div>
      <Countdown className={cls('timer')} dueTo={dueTo} />
      {isLinkFormVisible && (
        <div className={cls('link-form')}>
          <Form>
            <Form.Item noStyle>
              <Input placeholder={'Link'} />
            </Form.Item>
          </Form>
          <Row justify={'center'}>
            <Col span={16}>
              <Button
                className={cls('confirm-button')}
                size={'large'}
                type={'primary'}
                block
              >
                <b>Send Link</b>
                <span className={cls('confirm-button-subtext')}>
                  +{b4bReward}&nbsp;B4B&nbsp;Points
                </span>
              </Button>
            </Col>
          </Row>
        </div>
      )}
      {isActionsVisible && (
        <div className={cls('actions')}>
          <Button className={cls('reject-button')} size={'large'}>
            Reject
          </Button>
          <Button
            className={cls('confirm-button')}
            size={'large'}
            type={'primary'}
            onClick={() => {
              hashconnectService.initilaizeClient();
            }}
          >
            <b>Confirm</b>
            <span className={cls('confirm-button-subtext')}>
              +{b4bReward}&nbsp;B4B&nbsp;Points
            </span>
          </Button>
        </div>
      )}
    </div>
  );
};

export default AdCampaignCard;
