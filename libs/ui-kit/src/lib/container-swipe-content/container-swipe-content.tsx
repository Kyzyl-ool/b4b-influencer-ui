import './container-swipe-content.less';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Controller } from 'swiper';
import { useCls } from '../../hooks';
import React, { useEffect, useState } from 'react';
import { Swiper as SwiperClass } from 'swiper/types';
import ContentSwitcherHeader from '../content-switcher-header/content-switcher-header';
import { useLocation, useNavigate } from 'react-router-dom';

interface ContainerSwipeContentPage {
  content: React.ReactNode;
  key: string;
  title: string;
  route: string;
}

export interface ContainerSwipeContentProps {
  pages: ContainerSwipeContentPage[];
}

export const ContainerSwipeContent: React.FC<ContainerSwipeContentProps> = ({
  pages,
}) => {
  const cls = useCls('container-swipe-content');
  const [swiperInstance, setSwiperInstance] = useState<SwiperClass>();
  const { pathname } = useLocation();
  const navigate = useNavigate();

  const routeIndex = pages.findIndex(({ route }) => pathname.startsWith(route));

  const [currentPageIndex, setCurrentPageIndex] = useState(
    routeIndex !== -1 ? routeIndex : 0
  );

  useEffect(() => {
    if (routeIndex !== -1) {
      setCurrentPageIndex(routeIndex);
    }
  }, [routeIndex]);

  useEffect(() => {
    if (swiperInstance?.on) {
      swiperInstance.on('slideChange', (swiper) => {
        setCurrentPageIndex(swiper.activeIndex);
        navigate(pages[swiper.activeIndex].route);
      });
    }
  }, [navigate, pages, swiperInstance]);

  useEffect(() => {
    if (!swiperInstance) {
      return;
    }

    if (swiperInstance.activeIndex < currentPageIndex) {
      swiperInstance.slideNext();
    }
    if (swiperInstance.activeIndex > currentPageIndex) {
      swiperInstance.slidePrev();
    }
  }, [swiperInstance, currentPageIndex]);

  return (
    <>
      {pages[currentPageIndex].title && (
        <ContentSwitcherHeader
          currentTitle={pages[currentPageIndex].title}
          rightDisabled={currentPageIndex === 0}
          leftDisabled={currentPageIndex === pages.length - 1}
          onClickLeft={() => setCurrentPageIndex(currentPageIndex - 1)}
          onClickRight={() => setCurrentPageIndex(currentPageIndex + 1)}
        />
      )}
      <Swiper
        modules={[Controller]}
        className={cls()}
        onSwiper={setSwiperInstance}
      >
        {pages.map(({ content, key, title }) => (
          <SwiperSlide title={title} key={key}>
            {content}
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default ContainerSwipeContent;
