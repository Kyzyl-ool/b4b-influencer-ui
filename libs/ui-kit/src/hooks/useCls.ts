import { useContext } from 'react';
import { ConfigContext } from 'antd/lib/config-provider';
import { cn } from '@bem-react/classname';

export const useCls = (elemName: string) => {
  const { getPrefixCls } = useContext(ConfigContext);
  return cn(getPrefixCls(elemName));
};
