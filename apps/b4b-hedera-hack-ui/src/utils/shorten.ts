export const shorten = (address: string, firstNChars = 7, lastNChars = 5) => {
  if (firstNChars + lastNChars >= address.length) {
    return address;
  }

  const lastChars = address
    .split('')
    .reverse()
    .join('')
    .slice(0, lastNChars)
    .split('')
    .reverse()
    .join('');
  return address.slice(0, firstNChars) + '...' + lastChars;
};
