export const nonFalsyArray = <T>(array: T[]): T[] => {
  return array.filter((v) => !!v);
};
