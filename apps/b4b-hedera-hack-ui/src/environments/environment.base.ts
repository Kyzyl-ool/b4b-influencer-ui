export const environmentBase = {
  production: false,
  antdConfig: {
    theme: {
      primaryColor: '#8E4FFF',
      purpleBase: '#8E4FFF',
      cyanBase: '#00CFD9',
      blueBase: '#0085FF',
    },
    prefixCls: 'ant',
  },
};
