import './page-balance.less';
import { Button, Typography } from 'antd';
import {
  CoinsBalance,
  ContainerBaseLayer,
  ContainerBasePage,
  useCls,
} from '@b4b-hedera-hack/ui-kit';
import { useHashConnect } from '../../hooks/useHashConnect';
import { useState } from 'react';
import QRCode from 'react-qr-code';
import { hashconnectService } from '../../hashconnect';
import { useHashPackBalance } from '../../hooks/useHashPackBalance';
import { shorten } from '../../utils/shorten';

const { Text } = Typography;

/* eslint-disable-next-line */
export interface PageBalanceProps {}

export const PageBalance = (props: PageBalanceProps) => {
  const cls = useCls('page-balance');
  const [isConnectingHashConnect, setIsConnectingHashConnect] = useState(false);

  const { pairingString, isPaired } = useHashConnect();
  const hbarBalance = useHashPackBalance();

  return (
    <ContainerBasePage
      className={cls()}
      footer={
        <div className={cls('footer')}>
          <Button block type={'primary'} size={'large'}>
            Transfer
          </Button>
        </div>
      }
    >
      <Text className={cls('gray-text')}>Address</Text>
      <ContainerBaseLayer>
        {!hashconnectService.saveData.pairedAccounts[0] && (
          <Text type={'secondary'}>HashPack account not connected</Text>
        )}
        <b>{hashconnectService.saveData.pairedAccounts[0]}</b>
      </ContainerBaseLayer>
      <CoinsBalance
        coins={[
          { balance: '123', ticker: 'USDC' },
          { balance: '123.1', ticker: 'B4BP' },
          { balance: '123.01', ticker: 'B4BC' },
        ]}
      />
      {hbarBalance && (
        <CoinsBalance
          coins={[
            {
              balance: hbarBalance,
              ticker: 'HBAR',
            },
          ]}
        />
      )}
      {!isPaired && !isConnectingHashConnect && (
        <Button
          type={'primary'}
          size={'large'}
          onClick={() => setIsConnectingHashConnect(true)}
        >
          Connect HashPack Account
        </Button>
      )}
      {!isPaired && isConnectingHashConnect && pairingString && (
        <div className={cls('qr-code-container')}>
          <Text>Please scan the following QR-code to connect wallet:</Text>
          <QRCode value={pairingString} />
          <Text>Or paste the pairstring:</Text>
          <Text
            copyable={{
              text: pairingString,
            }}
          >
            <b>{shorten(pairingString)}</b>
          </Text>
        </div>
      )}
    </ContainerBasePage>
  );
};

export default PageBalance;
