import './container-calendar.less';
import React from 'react';
import { useCls } from '@b4b-hedera-hack/ui-kit';
import classnames from 'classnames';

export interface CalendarContainerProps
  extends React.HTMLProps<HTMLDivElement> {
  children: React.ReactNode;
}

export const ContainerCalendar: React.FC<CalendarContainerProps> = ({
  children,
  className,
}) => {
  const cls = useCls('calendar-container');

  return <div className={classnames(cls(), className)}>{children}</div>;
};

export default ContainerCalendar;
