import './page-ads-campaigns.less';
import {
  AdCampaignCard,
  ButtonGroupToggler,
  ContainerBasePage,
  useCls,
} from '@b4b-hedera-hack/ui-kit';
import { useState } from 'react';
import { DateTime } from 'luxon';

const colors: Record<number, string> = {
  0: 'purple',
  1: 'blue',
  2: 'cyan',
};

/* eslint-disable-next-line */
export interface PageAdsCampaignsProps {}

export function PageAdsCampaigns(props: PageAdsCampaignsProps) {
  const cls = useCls('page-ads-campaigns');
  const [currentTogglerIndex, setCurrentTogglerIndex] = useState(0);

  return (
    <ContainerBasePage className={cls()}>
      <div
        className={cls('background', {
          color: colors[currentTogglerIndex],
        })}
      />
      <ButtonGroupToggler
        currentTogglerIndex={currentTogglerIndex}
        togglers={[
          { name: 'New' },
          { name: 'Pending' },
          { name: 'In Progress' },
        ]}
        setCurrentTogglerIndex={setCurrentTogglerIndex}
      />
      <div className={cls('ads-container')}>
        <AdCampaignCard
          brandName={'NiceTry'}
          href={'https://google.com'}
          linkText={'https://google.com'}
          briefHref={'https:/google.com'}
          date={new Date()}
          reward={'200 $'}
          b4bReward={100}
          dueTo={DateTime.now().plus({ day: 2 }).toJSDate()}
          color={colors[currentTogglerIndex]}
          isActionsVisible={currentTogglerIndex === 0}
          isLinkFormVisible={currentTogglerIndex === 2}
        />
        <AdCampaignCard
          brandName={'NiceTry'}
          href={'https://google.com'}
          linkText={'https://google.com'}
          briefHref={'https:/google.com'}
          date={new Date()}
          reward={'200 $'}
          b4bReward={100}
          dueTo={DateTime.now().plus({ hours: 23 }).toJSDate()}
          color={colors[currentTogglerIndex]}
          isActionsVisible={currentTogglerIndex === 0}
          isLinkFormVisible={currentTogglerIndex === 2}
        />
        <AdCampaignCard
          brandName={'NiceTry'}
          href={'https://google.com'}
          linkText={'https://google.com'}
          briefHref={'https:/google.com'}
          date={new Date()}
          reward={'200 $'}
          b4bReward={100}
          dueTo={DateTime.now().plus({ hours: 2 }).toJSDate()}
          color={colors[currentTogglerIndex]}
          isActionsVisible={currentTogglerIndex === 0}
          isLinkFormVisible={currentTogglerIndex === 2}
        />
      </div>
    </ContainerBasePage>
  );
}

export default PageAdsCampaigns;
