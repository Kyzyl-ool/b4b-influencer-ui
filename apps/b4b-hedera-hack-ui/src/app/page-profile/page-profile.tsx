import './page-profile.less';
import {
  ContainerBaseLayer,
  LabelValue,
  useCls,
} from '@b4b-hedera-hack/ui-kit';
import { Typography } from 'antd';

const { Title } = Typography;

/* eslint-disable-next-line */
export interface PageProfileProps {}

export function PageProfile(props: PageProfileProps) {
  const cls = useCls('page-profile');

  return (
    <ContainerBaseLayer className={cls()}>
      <Title level={4} className={cls('header')}>
        Profile
      </Title>
      <LabelValue label={'Full Name'} value={'Kezhik Kyzyl-ool'} />
      <LabelValue label={'E-Mail'} value={'kyzyloolk@mail.ru'} />
    </ContainerBaseLayer>
  );
}

export default PageProfile;
