import React, { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';
import App from './app/app';
import './antd';
import './swiper';
import { ConfigProvider } from 'antd';
import { environment } from './environments/environment';
import { BrowserRouter } from 'react-router-dom';
import './hashconnect';

export function renderUI(element: Element, token: string) {
  const root = ReactDOM.createRoot(element);

  root.render(
    <StrictMode>
      <ConfigProvider prefixCls={environment.antdConfig.prefixCls}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ConfigProvider>
    </StrictMode>
  );
}

renderUI(document.getElementById('influencer-root') as HTMLElement, '');
