import { hashconnectService } from '../hashconnect';
import { useCallback, useEffect, useState } from 'react';

export const useHashConnect = () => {
  const [pairingString, setPairingString] = useState<string>();
  const [isPaired, setIsPaired] = useState(false);

  const logStatus = useCallback(() => {
    switch (hashconnectService.status) {
      case 'Connected': {
        setPairingString(hashconnectService.saveData.pairingString);
        break;
      }
      case 'Paired': {
        console.log('Paired!');
        setIsPaired(true);
        break;
      }
    }
  }, []);

  useEffect(() => {
    logStatus();
    hashconnectService.on('status', logStatus);

    return () => {
      hashconnectService.removeListener('status', logStatus);
    };
  }, [logStatus]);

  return {
    pairingString,
    isPaired,
  };
};
