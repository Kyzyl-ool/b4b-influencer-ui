// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import React from 'react';

export const SwiperContext = React.createContext(null);
